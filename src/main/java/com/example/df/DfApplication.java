package com.example.df;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@MapperScan(basePackages = {"com.example.df.mapper"})
public class DfApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(DfApplication.class, args);
    }
}
