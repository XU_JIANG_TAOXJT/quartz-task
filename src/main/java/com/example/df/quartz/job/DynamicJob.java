package com.example.df.quartz.job;

import com.example.df.util.QuartzreflectUtil;
import com.example.df.util.SpringContextUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

/**
 * @Author: xu
 * @Description: TODO	定时任务需要实现的接口
 * @Date: 2021/11/29 15:39
 */
public interface DynamicJob<T> extends Job
{
	/**
	 * 需要执行任务的方法体
	 * @param args	可变参数，前端传递
	 * @return	object
	 */
	@SuppressWarnings("all")
	T task(Object... args);

	@Override
	default void execute(JobExecutionContext context) {
		QuartzreflectUtil quartzreflectUtil = SpringContextUtil.getBean(QuartzreflectUtil.class);
		quartzreflectUtil.reflect(context);
	}
}
