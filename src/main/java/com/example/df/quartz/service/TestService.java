package com.example.df.quartz.service;

import com.example.df.mapper.UserMapper;
import com.example.df.quartz.job.DynamicJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author xu
 * @Description 一个测试的业务方法
 * @createTime 2021年03月11日 14:37:00
 */
@Component
public class TestService implements DynamicJob<String>
{
    @Autowired
    UserMapper userMapper;

    /**
     * 定时任务执行的方法
     * @param args	可变参数，前端传递,当无参时为null
     * @return 根据需要，由泛型定义
     */
    @Override
    @SuppressWarnings("unchecked")
    public String task(Object... args)
    {
        String a =(String) args[0];
        Integer cc =(Integer) args[1];
        Map<String,Object> map = (Map<String,Object>) args[2];
        for (String o : map.keySet()) {
            System.out.println(o+"\t"+map.get(o));
        }
        System.out.println(cc);
        System.out.println(a);
        Integer count = userMapper.getCount();
        System.out.println(count);
        return count.toString();
    }
}
