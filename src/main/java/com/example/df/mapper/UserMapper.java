package com.example.df.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: xu
 * @Description: TODO
 * @Date: 2021/11/19 21:00
 */
@Mapper
public interface UserMapper
{
	Integer getCount();
}
