# QuartzTask

#### 介绍
SpringBoot整合Quartz动态任务管理后台



> 由于使用的是反射的方式加载类，并没有交由spring容器管理，如果定时任务方法需要操作数据库、等等注入的一些类...会导致失效，
> 可以使用原生jdbc方式操作数据库。
>


>    **v2.0版本支持操作数据库，统一交由spring容器管理。简化操作**   

**查看详细信息请访问**
[定时任务后台管理博客](https://blog.csdn.net/languageStudent/article/details/114662825)

#### 软件架构
软件架构说明
项目中存在mysql初始化sql，其他数据库初始化文件请参考quartz官网

#### 特技

2、更改配置文件的数据库路径及用户

3、更改service和controller的PackageUrl的包地址前缀（根据自己的项目来）

启动SpringBOOT启动类
调用添加接口
参数格式为json
例：



```
{
"className":"testService",
"jobName":"job2",
"jobGroupName":"test",
"cron":"0/5 * * * * ?",					每五秒执行一次
"method":"test"  (v1.0需要传，v2.0不用传)
}

```

有参数的方法与类型调用如下：
```
{
"className":"testService",        需要执行方法的类名（首字母小写，根据spring容器获取对象）
"jobName":"job2",        任务名（随意，可自定义。）
"jobGroupName":"test",        任务分组名（随意，可自定义。）		任务的暂停、执行、删除。。。都是根据任务名和分组名来确定任务
"cron":"0/5 * * * * ?",        执行时间（cron时间表达式）
"method":"test2",        (v1.0需要传，v2.0不用传)需要执行方法名
"paramData":{			该方法需要接收的参数，根据方法传递
   {"String":"test"},
   {"Map":{"cc":"ss","count":5}},
   {"Integer":5}
  }	   方法参数数据，"String表示数据类型"	value为具体数据
}

```



测试TestService添加定时任务请求

```json
{
    "jobGroupName":"test",
    "jobName":"test",
    "className":"testService",
    "cron":"0/5 * * * * ?",
    "paramData":[
        {
            "String":"test"
        },

        {
            "Integer":5
        },
        {
            "Map":{"cc":"ss","count":5}
        }
    ]
}
```
